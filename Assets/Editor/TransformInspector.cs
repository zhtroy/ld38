using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(Transform))]
public class TransformInspector : Editor
{
	public override void OnInspectorGUI ()
	{
		Transform trans = target as Transform;
		EditorGUIUtility.LookLikeControls(15f);

		Vector3 pos;
		Vector3 rot;
		Vector3 scale;

		// Position
		EditorGUILayout.BeginHorizontal();
		{
			if (DrawButton("P", "Reset Position", IsResetPositionValid(trans), 20f))
			{
				Undo.RegisterUndo(trans, "Reset Position");
				trans.localPosition = Vector3.zero;
			}
			pos = DrawVector3(trans.localPosition);
		}
		EditorGUILayout.EndHorizontal();

		// Rotation
		EditorGUILayout.BeginHorizontal();
		{
			if (DrawButton("R", "Reset Rotation", IsResetRotationValid(trans), 20f))
			{
				Undo.RegisterUndo(trans, "Reset Rotation");
				trans.localEulerAngles = Vector3.zero;
			}
			rot = DrawVector3(trans.localEulerAngles);
		}
		EditorGUILayout.EndHorizontal();

		// Scale
		EditorGUILayout.BeginHorizontal();
		{
			if (DrawButton("S", "Reset Scale", IsResetScaleValid(trans), 20f))
			{
				Undo.RegisterUndo(trans, "Reset Scale");
				trans.localScale = new Vector3(1,1,1);
			}
			scale = DrawVector3(trans.localScale);
		}
		EditorGUILayout.EndHorizontal();

		// 如果有数值更改，设置 transform 数值
		if (GUI.changed)
		{
			Undo.RegisterUndo(trans, "Transform Change");
			trans.localPosition		= Validate(pos);
			trans.localEulerAngles	= Validate(rot);
			trans.localScale		= Validate(scale);
		}
	}

	static bool DrawButton (string title, string tooltip, bool enabled, float width)
	{
		if (enabled)
		{
			Color color = GUI.color;
			GUI.color = new Color(1f, 0.5f, 0.5f, 1f);
			bool hexart = GUILayout.Button(new GUIContent(title, tooltip), GUILayout.Width(width));
			GUI.color = color;
			return hexart;
		}
		else
		{
			Color color = GUI.color;
			GUI.color = new Color(1f, 0.5f, 0.5f, 0.25f);
			GUILayout.Button(new GUIContent(title, tooltip), GUILayout.Width(width));
			GUI.color = color;
			return false;
		}
	}

	static Vector3 DrawVector3 (Vector3 value)
	{
		GUILayoutOption opt = GUILayout.MinWidth(30f);
		value.x = EditorGUILayout.FloatField("X", value.x, opt);
		value.y = EditorGUILayout.FloatField("Y", value.y, opt);
		value.z = EditorGUILayout.FloatField("Z", value.z, opt);
		return value;
	}

	static bool IsResetPositionValid (Transform targetTransform)
	{
		Vector3 v = targetTransform.localPosition;
		return (v.x != 0f || v.y != 0f || v.z != 0f);
	}

	static bool IsResetRotationValid (Transform targetTransform)
	{
		Vector3 v = targetTransform.localEulerAngles;
		return (v.x != 0f || v.y != 0f || v.z != 0f);
	}

	static bool IsResetScaleValid (Transform targetTransform)
	{
		Vector3 v = targetTransform.localScale;
		return (v.x != 1f || v.y != 1f || v.z != 1f);
	}

	static Vector3 Validate (Vector3 vector)
	{
		vector.x = float.IsNaN(vector.x) ? 0f : vector.x;
		vector.y = float.IsNaN(vector.y) ? 0f : vector.y;
		vector.z = float.IsNaN(vector.z) ? 0f : vector.z;
		return vector;
	}
}

public class SelectionTools
{
	[MenuItem("Hexart/Toggle 'Active' #&a")]
	static void ActivateDeactivate()
	{
		if (HasValidTransform())
		{
			GameObject[] gos = Selection.gameObjects;
			bool val = !Selection.activeGameObject.activeSelf;
			foreach (GameObject go in gos) go.SetActive(val);
		}
	}

	[MenuItem("Hexart/Clear Local Transform #&c")]
	static void ClearLocalTransform()
	{
		if (HasValidTransform())
		{
			Undo.RegisterSceneUndo("Clear Local Transform");

			Transform t = Selection.activeTransform;
			t.localPosition = Vector3.zero;
			t.localRotation = Quaternion.identity;
			t.localScale = Vector3.one;
		}
	}

	[MenuItem ("Hexart/Add New Child #&n")]
	static void CreateLocalGameObject()
	{
		if (PrefabCheck())
		{
			// 无法撤销
			Undo.RegisterSceneUndo("Add New Child");
			GameObject newGameObject = new GameObject();
			newGameObject.name = "GameObject";

			if (Selection.activeTransform != null)
			{
				newGameObject.transform.parent = Selection.activeTransform;
				newGameObject.name = "Child";
				newGameObject.transform.localPosition = Vector3.zero;
				newGameObject.transform.localRotation = Quaternion.identity;
				newGameObject.transform.localScale = new Vector3(1f, 1f, 1f);
				newGameObject.layer = Selection.activeGameObject.layer;
			}
			Selection.activeTransform = newGameObject.transform;
		}
	}

	[MenuItem("Hexart/List Dependencies #&i")]
	static void ListDependencies()
	{
		if (HasValidSelection())
		{
			Debug.Log("Asset dependencies:\n" + GetDependencyText(Selection.objects));
		}
	}

#region Helper Functions
	
	class AssetEntry
	{
		public string path;
		public List<System.Type> types = new List<System.Type>();
	}

	static bool HasValidSelection()
	{
		if (Selection.objects == null || Selection.objects.Length == 0)
		{
			Debug.LogWarning("You must select an object first");
			return false;
		}
		return true;
	}

	static bool HasValidTransform()
	{
		if (Selection.activeTransform == null)
		{
			Debug.LogWarning("You must select an object first");
			return false;
		}
		return true;
	}

	static bool PrefabCheck()
	{
		if (Selection.activeTransform != null)
		{
			// 如果是 prefab 实例则警告
			PrefabType type = PrefabUtility.GetPrefabType( Selection.activeGameObject );

			if (type == PrefabType.PrefabInstance)
			{
				return EditorUtility.DisplayDialog("Losing prefab",
					"This action will lose the prefab connection. Are you sure you wish to continue?",
					"Continue", "Cancel");
			}
			return true;
		}
		return false;
	}

	static List<AssetEntry> GetDependencyList (Object[] objects)
	{
		Object[] deps = EditorUtility.CollectDependencies(objects);
		
		List<AssetEntry> list = new List<AssetEntry>();
		
		foreach (Object obj in deps)
		{
			string path = AssetDatabase.GetAssetPath(obj);
			
			if (!string.IsNullOrEmpty(path))
			{
				bool found = false;
				System.Type type = obj.GetType();
				
				foreach (AssetEntry ent in list)
				{
					if (string.Equals(ent.path, path))
					{
						if (!ent.types.Contains(type)) ent.types.Add(type);
						found = true;
						break;
					}
				}
				
				if (!found)
				{
					AssetEntry ent = new AssetEntry();
					ent.path = path;
					ent.types.Add(type);
					list.Add(ent);
				}
			}
		}
		
		deps = null;
		objects = null;
		return list;
	}

	static string RemovePrefix (string text)
	{
		text = text.Replace("UnityEngine.", "");
		text = text.Replace("UnityEditor.", "");
		return text;
	}

	static string GetDependencyText (Object[] objects)
	{
		List<AssetEntry> dependencies = GetDependencyList(objects);
		List<string> list = new List<string>();
		string text = "";

		foreach (AssetEntry ae in dependencies)
		{
			text = ae.path.Replace("Assets/", "");
			
			if (ae.types.Count > 1)
			{
				text += " (" + RemovePrefix(ae.types[0].ToString());
			
				for (int i = 1; i < ae.types.Count; ++i)
				{
					text += ", " + RemovePrefix(ae.types[i].ToString());
				}
				
				text += ")";
			}
			list.Add(text);
		}
		
		list.Sort();
		
		text = "";
		foreach (string s in list) text += s + "\n";
		list.Clear();
		list = null;
		
		dependencies.Clear();
		dependencies = null;
		return text;
	}
#endregion
}
