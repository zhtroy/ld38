﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
	public static SoundManager instance = null;

	public AudioSource audioSource;
	public AudioClip clipGetMoney;
	public AudioClip clipLoseMoney;
	public AudioClip clipSelect;
	// Use this for initialization
	void Awake () {
		if (instance == null) {
			instance = this;
		} else {
			Destroy (gameObject);
		}
	}
	
	public void PlayClip(AudioClip clip){
		audioSource.clip = clip;
		audioSource.Play ();
	}
}
