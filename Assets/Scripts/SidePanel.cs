﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SidePanel : MonoBehaviour {

	public Text nameText;
	public Text ageText;
	public Text jobText;
	public Text hobbyText;
	public Text psText;
	public Text sentenceText;
	public Button befriendBtn;
	public Text moneyText;

	Node _currentNode;
	// Use this for initialization
	void Start () {
		
	}

	public void SetCurrentNode(Node node){
		_currentNode = node;
		UpdateView ();
	}

	public void OnBefriend(){
		if (_currentNode == null) {
			return;
		}
		_currentNode.Befriend ();

		UpdateView ();
	}

	void UpdateView(){
		if (_currentNode == null) {
			return;
		}
		ShowPersonInfo (_currentNode.info);
		if (_currentNode.IsFriend || _currentNode.CompareTag("goal")) {
			befriendBtn.gameObject.SetActive (false);
		} else {
			befriendBtn.gameObject.SetActive (true);
		}
	}
	void ShowPersonInfo(PersonInfo info){
		nameText.text = info.name;
		ageText.text = info.age;
		jobText.text = info.job;
		hobbyText.text = info.hobby;
		psText.text = info.PS;
		sentenceText.text = info.sentence;
	}



	public void SetChances(int num){
		if (num<0) {
			return;
		}
		moneyText.text = num.ToString();
	}
}
