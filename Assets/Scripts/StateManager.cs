﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour {
	public static StateManager instance = null;

	public enum State
	{
		FirstTime,
		Win,
		Lose

	}
	public State GameState{ get; set; }

	// Use this for initialization
	void Awake () {
		if (instance == null) {
			instance = this;

		} else {
			Destroy (gameObject);

		}
		DontDestroyOnLoad (gameObject);
		GameState = State.FirstTime;
	}


}
