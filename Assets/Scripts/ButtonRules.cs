﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonRules : MonoBehaviour {
	public GameObject rulesPanel;
	public Text btnText;

	bool _isOn= false;

	void Awake(){
		rulesPanel.SetActive (false);
	}
	public void Toggle(){
		if (_isOn) {
			rulesPanel.SetActive (false);
			btnText.text = "Rules:off";
			_isOn = false;
		} else {
			rulesPanel.SetActive (true);
			btnText.text = "Rules:on";
			_isOn = true;
		}
	}

}
