﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	public static GameManager instance = null;
	public int maxDegrees = 6;
	public SidePanel sidePanel;
	public Image fadeImage;
	public Button rulesBtn;

	#region pro
	int _chances; 
	public int Money{
		set{
			_chances = value;
			if (_chances<=0) {
				EndScene (false);

			}
			sidePanel.SetChances (_chances);
		}	
		get{
			return _chances;
		}
	}
	#endregion

	public void EndScene(bool win){
		fadeImage.gameObject.SetActive (true);
		StateManager.instance.GameState = win ? StateManager.State.Win : StateManager.State.Lose;
		fadeImage.DOFade (1, 2).SetEase(Ease.Linear).OnComplete(()=>{SceneManager.LoadScene("menu");});
	}

	void Awake(){
		if (instance == null) {
			instance = this;
		} else {
			DestroyObject (gameObject);
		}
		_chances = maxDegrees+1;

	}

	void Start(){
		if (StateManager.instance.GameState == StateManager.State.FirstTime) {
			rulesBtn.onClick.Invoke ();
		}

	}
		

}
