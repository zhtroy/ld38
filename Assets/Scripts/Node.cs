﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PersonInfo{
	public string name;
	public string age;
	public string job;
	public string hobby;
	public string PS;
	public string sentence;
}

public class Node : MonoBehaviour {
	public bool isGoal = false;
	public bool startActive = false;
	public GameObject prefabLine;
	public List<Node> relatedNodes;
	public PersonInfo info;
	public bool onlyBenifitFromGoodNews;
	public NewsType[] goodNews ;
	public NewsType[] badNews;
	public string[] goodNewsString;
	public string[] badNewsString;
	public int goodNewBonous = 1;
	public int badNewsPenalty = 2;
	public GameObject prefabSelected;




	public bool IsFriend {
		get;
		private set;
	}
	SidePanel _sidePanel;
	PopUpNews _popUp;
	Transform _blinkingSquare;
	// Use this for initialization
	void Awake () {
		gameObject.SetActive (startActive);
		_sidePanel = GameObject.Find ("SidePanel").GetComponent<SidePanel> ();
		_popUp = GameObject.Find ("PopUpNews").GetComponent<PopUpNews> ();
		_blinkingSquare = GameObject.Find ("blinkingSquare").transform;

	}

	void Start(){
		if(gameObject.CompareTag("start")){
			Befriend();
			OnMouseDown ();
		
		}
	}

	void OnMouseDown(){
		_blinkingSquare.position = transform.position;
		_sidePanel.SetCurrentNode (this);
		SoundManager.instance.PlayClip (SoundManager.instance.clipSelect);

	}

	bool OpenNode(){
		bool ret = false;
		LineRenderer line;
		Vector3[] vertexes = new Vector3[2];
		vertexes [0] = transform.position;

		foreach (var node in relatedNodes) {
			if (node != null) {
				if (node.IsFriend) {
					continue;
				}
				vertexes [1] = node.transform.position;
				line = ((GameObject) Instantiate (prefabLine, Vector3.zero, Quaternion.identity)).GetComponent<LineRenderer>();
				line.SetPositions (vertexes);
				if (node.isGoal) {
					ret = true;
				} else {
					node.gameObject.SetActive (true);
				}

			}

		}
		IsFriend = true;
		return ret;

	}
	int FindInArray(NewsType[] array,NewsType item){
		for (int i = 0; i < array.Length; i++) {
			if (array [i] == item) {
				return i;
			}
		}
		return -1;

	
	}
	public void Befriend(){
		if (IsFriend) {
			return;
		}
		bool win = OpenNode ();
		if (onlyBenifitFromGoodNews) {
			int idx;
			if ((idx = FindInArray(goodNews,NewsManager.instance.CurrentNewsType)) !=-1) {
				GameManager.instance.Money += goodNewBonous;
				_popUp.ShowResult (goodNewsString[idx], goodNewBonous);
			} else {
				GameManager.instance.Money -= badNewsPenalty;
				_popUp.ShowResult (badNewsString[0],-badNewsPenalty);
			}
		} else {
			int idx;
			if ( (idx = FindInArray(goodNews,NewsManager.instance.CurrentNewsType))!=-1) {
				GameManager.instance.Money += goodNewBonous;
				_popUp.ShowResult (goodNewsString[idx],goodNewBonous);
			} else if ((idx = FindInArray(badNews,NewsManager.instance.CurrentNewsType)) != -1) {
				GameManager.instance.Money -= badNewsPenalty;
				_popUp.ShowResult (badNewsString[idx],-badNewsPenalty);
			} else {
				GameManager.instance.Money--;
				if (tag != "start") {
					_popUp.ShowNormalResult (info.name, -1);
				}
			}
		}
		if (tag != "start") {
			Instantiate (prefabSelected, transform.position, Quaternion.identity);
		}


		if (win) {
			GameManager.instance.EndScene (true);
		}
	}
	
	void OnDrawGizmosSelected(){
		foreach (var item in relatedNodes) {
			if (item !=null) {
				Gizmos.DrawLine (transform.position, item.transform.position);
			}
		
		}
	}
}
