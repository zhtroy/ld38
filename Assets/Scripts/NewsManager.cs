﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public enum NewsType 
{
	none = 0,
	sunny,
	rainy,
	policeReduce,
	AAAgamesales,
	indieMarket,
	doctorPayRaise,
	checkCorruption,
	moreMusic,
	learningGolf,
	zeldaGovernment,
	zeldaReleased,
	bookpraised,
	lessPress,
	last

}
public class NewsManager : MonoBehaviour {
	public static NewsManager instance = null;

	public float newsTime;
	public Text rollingText;
	public string[] newsString;
	public NewsType CurrentNewsType {
		get;
		set;
	}

	RectTransform boundRectTransform;
	// Use this for initialization
	void Awake () {
		if (instance == null) {
			instance = this;
		} else {
			DestroyObject (gameObject);
		}
		boundRectTransform = GetComponent<RectTransform> ();
		StartRollingNews ();

	}

	void StartRollingNews(){
		//Choose a random news
		int range = (int) NewsType.last;
		int r = Random.Range (1, range);
		CurrentNewsType = (NewsType)r;
		r = r - 1;
		rollingText.text = newsString [r];

		rollingText.rectTransform.anchoredPosition = new Vector2(boundRectTransform.rect.xMax*2, 0);
		rollingText.rectTransform.DOAnchorPosX (boundRectTransform.rect.xMin*2, newsTime).SetEase(Ease.Linear). OnComplete (StartRollingNews);
	}

	
		






}
