﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpNews : MonoBehaviour {

	public Text resultText;
	public Text coinNumText;
	public Color coinUpColor;
	public Color coinDownColor;

	Animator _animator;
	string[] _part1;
	string[] _part2;
	// Use this for initialization
	void Start () {
		_animator = GetComponent<Animator> ();
		_part1 = new string[4];
		_part2 = new string[4];
		_part1 [0] = "You buy ";
		_part1 [1] = "You give ";
		_part1 [2] = "You invite ";
		_part1 [3] = "You buy ";

		_part2 [0] = " a flower.";
		_part2 [1] = " a present.";
		_part2 [2] = " to dinner.";
		_part2 [3] = " a Zelda game.";

	}

	public void ShowResult(string result, int coinDelta)
	{
		string coinString = coinDelta.ToString ();
		if (coinDelta>0) {
			coinNumText.color = coinUpColor;
			coinString = "+" + coinString;
			SoundManager.instance.PlayClip (SoundManager.instance.clipGetMoney);
		} else if (coinDelta<0) {
			coinNumText.color = coinDownColor;
			SoundManager.instance.PlayClip (SoundManager.instance.clipLoseMoney);
		}

		coinNumText.text = coinString;

		resultText.text = result;
		_animator.SetTrigger ("start");
	}

	public void ShowNormalResult(string name, int coinDelata){
		int r = Random.Range (0, _part1.Length);
		string result = _part1 [r] + name + _part2 [r];
		ShowResult (result, coinDelata);
	}

}
