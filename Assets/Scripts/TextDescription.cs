﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextDescription : MonoBehaviour {
	public Text desText;
	public string firstTimeStr;
	public string loseStr;
	public string winStr;
	// Use this for initialization
	void Start () {
		switch (StateManager.instance.GameState) {
		case StateManager.State.FirstTime:
			desText.text = firstTimeStr;
			break;
		case StateManager.State.Win:
			desText.text = winStr;
			break;
		case StateManager.State.Lose:
			desText.text = loseStr;
			break;
		default:
			break;
		}
	}
	

}
